---
layout: page
title: RKWard on macOS
---

# RKWard on macOS

* Table of contents
{:toc}

## Status

Although access to a Mac is still limited for the main contributors, as
of version 0.7.0, there is a pre-compiled ready-to-install bundle for
Mac OS X \>= 10.11. We want to encourage you to send feedback to our
[**mailing lists**](Contact.html), be it success stories or bug
reports, because without it is hard to continue support for this
operating system in the future\!

Please take the information below as a guide through the required steps.
If one of the steps is unclear, or simply does not work, [**let us
know**](Contact.html), and we will find a solution.

## Installing using the precompiled binary bundle

We've used the MacPort of RKWard to precompile installation bundles wich
include all needed KDE libraries and RKWard (currently only for Mac OS X
El Capitan (10.11) or above):

  - <https://download.kde.org/stable/rkward/0.7.0/mac/RKWard-binary-0.7.0b_OSX10.11_KF5-5.42.0_needs_CRAN_R-3.4.4.pkg>
    (for R 3.4.4)
  - <https://download.kde.org/stable/rkward/0.7.0/mac/RKWard-binary-0.7.0b_OSX10.11_KF5-5.42.0_needs_CRAN_R-3.5.0.pkg>
    (for R 3.5.0)

These bundles do not include R, which means you need to install the
appropriate version of [R from
CRAN](http://cran.r-project.org/bin/macosx/) first (**make sure the
RKWard bundle you install matches this R version**, as is said in the
bundle's file name: "*needs_CRAN_R-<version>*"). The bundle is still
work in progress, but it has been tested and is reported to run on
various machines. A screencast of the installation is available
[here](http://www.youtube.com/watch?v=2t4zUOlgPG8).

**Note:** *You might be prompted the installer doesn't come from a
trusted source. After you confirmed you would like to install it anyway,
OS X/macOS should remember this decision, but there have been cases
where the installer needed to be started from the beginning again.*

If you use this bundle package, please [send us
feedback](Contact.html), be it a short success notice or a bug
report. We rely on this simple feedback for the further development of
this port\!

### Uninstalling the bundle

To uninstall RKWard, drag the app from your Applications to Trash. An
uninstaller dialog should pop up and offer to remove the app completely
(do a quick check that it offers to remove */opt/rkward*).
Alternatively, you can also run the uninstaller directly from your
personal Applications folder (in your user's home directory).

If you don't want to use the uninstaller, you can also remove RKWard
manually by following the [instructions on uninstalling the MacPorts
source installation](#Uninstalling_RKWard.html).

## Installing from source using MacPorts

The [MacPorts project](http://www.macports.org) offers up-to-date KDE
packages for Mac OS X, so at the moment it is considered the preferred
method over fink (see [earlier revisions of this
page](https://rkward.kde.org/index.php?title=RKWard_on_Mac&oldid=1419)).
A screencast of the process is available
[here](http://www.youtube.com/watch?v=OiAsSyNo_NY&feature=plcp).

### When is this preferable over the binary bundle?

Because the bundling process currently involves a lot of manual steps,
we can only release new binary bundles every now and then. If you need
or want to check out the latest development version of RKWard, e.g., to
test new features or fixes, installing the latest port is the preferred
method for Mac users. However, *you should not mix both installation
types*, allthough the bundle should not interfere with MacPorts in
general.

### Pre-Requirements

1.  Install all requirements for the [MacPorts
    software](http://www.macports.org/install.php). Please make sure you
    have a *recent* version of XCode installed and take care of keeping
    it up-to-date, because some ports will not compile otherwise. In
    case you have more than one installation of XCode on your system,
    make sure to set one (probably the latest) as the default, because
    confusion about the gcc compilers available can mess up the build. A
    symptom for this might be failing builds of certain ports later on.
    If this is the case, use `xcode-select` to set the default before
    installing MacPorts, e.g.:
        xcode-select -switch /Applications/Xcode.app/Contents/Developer
2.  The currently recommended method is to use the build script from
    RKWard's most recent source distribution. It can setup your machine,
    install [MacPorts](https://www.macports.org/install.php) and then
    one of the supported RKWard ports with all its dependencies. We do
    recommend this way because the script offers the most recent build
    process, which is sometimes ahead of this wiki page, and it's also
    the method we use to build and bundle RKWard for OS X. **Be aware
    that by default the script installs everything to /opt/rkward, which
    is not the MacPorts default path\! Do not use the script if you're
    already using MacPorts\! In that case, try the official [RKWard
    port](https://trac.macports.org/browser/trunk/dports/kde/rkward/Portfile)
    instead.**
    To proceed, open a terminal and follow these steps:

<!-- end list -->

1.  Fetch the latest version of the build script and make it executable:
        curl https://cgit.kde.org/rkward.git/plain/macports/update_bundle.sh -o update_bundle.sh
        chmod +x update_bundle.sh
2.  Run the script without arguments to get a list of all options:
        ./update_bundle.sh
3.  As you can see on top of the output, there's a small step-by-step
    guide to get you started. These are the most usual steps:
        ./update_bundle.sh -G       # setup the basic build environment
        ./update_bundle.sh -F 2.4.2 # install MacPorts version 2.4.2 (replace with the most recent version!)
        ./update_bundle.sh -Q       # build Qt and macOS integration for KF5
    Be aware that especially the last step can take many, many hours to
    compile -- depending on your CPU, even days\!

All further needed software packages (KF5 and possibly R) will be
installed with the RKWard port automatically, so you should need no
additional preparations. You can now actually remove the script you
downloaded, because the *-G* option saved a copy to *\~/bin* and added
it to your path.

### Building RKWard

This step also only takes one call in a terminal, but a lot of time to
finish. With the steps described above you have prepared MacPorts to
build the package.

The script allows you to decide whether you would like to build against
a present installation of R from CRAN, or use MacPorts version of R. You
can also choose between the latest stable release of RKWard or the
current development snapshot **(meaning: this is the bleeding edge
approach\!)** etc. To install RKWard stable with CRAN R, call the script
with these options: \~/bin/update_bundle.sh -r -D0 \# build RKWard, -D0
deactivates the default development version

If, on the other hand, you want to install the official port from a
standard MacPorts installation, you should use this command: sudo port
-v install rkward

### Running RKWard

If the compilation and installation finished successfully, you should
now find RKWard in your Applications menu.

### Uninstalling RKWard

If you installed RKWard using the installer package, your installation
should include an uninstaller which either pops up when you drag the app
to Trash, or can be started manually from your user's Application menu.

For the manual MacPorts builds, there is no genuine uninstall routine
yet. But to remove it again from your system, all you need to do is to
remove two directories:
```bash
sudo rm -rf /Applications/RKWard
sudo rm -rf /opt/rkward
```

**Note:** Of course, in case you have saved anything manually to these
folders since installation, backup your data first -- and be careful
with `rm -rf`\!

Alternatively, you can also have this done by the build script to wipe
RKWard from your system: `~/bin/update_bundle.sh -X`

Using the script will also clean up some symlinks that were created for
KDE services during installation. Removing RKWard manually will leave
these symbolic links. They will do no harm, since you've just removed
their targets. However, if you're certain you're not running KDE
services anymore, you can also clean those up manually: sudo rm
/Library/LaunchDaemons/org.freedesktop.dbus-system.plist sudo rm
/Library/LaunchDaemons/org.freedesktop.avahi-daemon.plist sudo rm
/Library/LaunchDaemons/org.freedesktop.avahi-dnsconfd.plist sudo rm
/Library/LaunchAgents/org.freedesktop.dbus-session.plist sudo rm
/Library/LaunchAgents/org.macports.kdecache.plist

**Note:** To be safe here, first check with `ls -l
/Library/LaunchDaemons` and `ls -l /Library/LaunchAgents` that these
links are really dead and associated with files in `/opt/rkward`\!

## Troubleshooting

Again: Some detail or other of the above instructions is probably just
wrong. If / when you get an error, please don't give up. Send us a
[mail](Contact.html), and we will talk you through. BTW: If you
figured out the remaining details all by yourself: Great\! But please
drop us a note as well, so we can provide more accurate instructions in
the future.

### RKWard crashes on start

**Possible solution no. 1:** Your KDE setup is probably not ready, most
likely dbus is not running. Before you can start any KDE software, you
first need to run the following commands once: sudo launchctl load -w
/Library/LaunchDaemons/org.freedesktop.dbus-system.plist launchctl load
-w /Library/LaunchAgents/org.freedesktop.dbus-session.plist

**Possible solution no. 2:** You had installed R from another source
before, RKWard picks the wrong one and doesn't find its own R
package(s). If possible, remove the older R installation first.

### Language is always english

**Possible solution:** If the `LANG` variable is not set in the
environment RKWard is started, you will always get the english
interface. To change that, e.g. into german, you can also use
`launchctl`, before you start RKWard: launchctl setenv LANG de_DE.UTF-8
This setting takes effect immediately, but won't survive a reboot. A
more permanent solution is to set this environment variable for your
user account by creating or adding to the
file`~/.MacOSX/environment.plist`:

    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
    <dict>
        <key>LANG</key>
        <string>de_DE.UTF-8</string>
    </dict>
    </plist>
