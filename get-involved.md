---
layout: page
title: "Get Involved in RKWard"
---

# You can help

RKWard is developed by a community of volunteers, working on the project in their free time. You can help us in various ways: 

* [Donate](http://sourceforge.net/donate/index.php?group_id=50231)
* Learn how to create your own plugins (graphical dialogs for R functionality): [Writing plugins](https://rkward.sourceforge.net/documents/devel/plugins/index.html)
* Learn about the many other development tasks, you can help with. [Development](/Developer_Information.html)
* [Translate RKWard](/Translation_RKWard.html) to your native language.
* [Report and review bugs](/Bugs.html)
* Update, extend, and improve this website:
  1. Get a [KDE identity account](https://identity.kde.org)
  2. Go to [the RKWard website repository](https://invent.kde.org/websites/rkward-kde-org) and click "Fork"
  3. You can edit the pages in your fork using the online editor (or the regular git workflow)
  4. When done, create a merge request for the main repository
