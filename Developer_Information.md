---
layout: page
---

# Developer information

This page has information on what you can do to help, and where to get started. Contents of this page:

* Table of contents
{:toc}

## Important starting points for all developers

  - [RKWard Source Repository](RKWard_Source_Repository.html): For
    developers it is highly useful to always keep track of the current
    state of development, not only when offical versions are released.
  - [Open Tasks](Open_Tasks.html)
  - [Feature Plan](Feature_Plan.html)
  - [Release Schedule](Release_Schedule.html)

## Plugin developers

  - Basic documentation on writing plugins: [HTML for online
    browsing](http://api.kde.org/doc/rkwardplugins)
  - [Development Status of
    Plugins](Development_Status_of_Plugins.html)
  - [Plugins under planning](Plugins_under_planning.html)
  - [Automated Plugin Testing](Automated_Plugin_Testing.html)

## Translators

  - [Getting started with translating RKWard](Translation_RKWard.html)

## Documentation Writers

### Enhancing this Website

  - Update, extend, and improve this website:
    1. Get a [KDE identity account](https://identity.kde.org)
    2. Go to [the RKWard website repository](https://invent.kde.org/websites/rkward-kde-org) and click "Fork"
    3. You can edit the pages in your fork using the online editor (or the regular git workflow)
    4. When done, create a merge request for the main repository

### Documentation inside RKWard

This chapter in the documentation on writing plugins provides some
basics:
<http://rkward.sourceforge.net/documents/devel/plugins/pluginhelp.html>

## Marketing

  - [News Coverage](News_Coverage.html)

## C++-coders

  - [Online API documentation](http://api.kde.org/playground-api/edu-apidocs/rkward/html/index.html)

<!-- end list -->

  - The appendix to our article in the Journal of Statistical Software discusses several aspects of the techical design, and is still mostly relevant: ([Abstract](http://www.jstatsoft.org/v49/i09), [PDF](http://www.jstatsoft.org/v49/i09/paper)).

## Resources

The main bug tracker / feature request tracker is here, and if you want
to report issues in RKWard, please use these:

### External trackers

However, there are a number of further places where bugs / issues are
reported. While these trackers are distribution-specific, many of the
issues reported there concern RKWard in general. Therefore it makes
sense to a least check these once in a while:

  - Ubuntu: <https://bugs.launchpad.net/ubuntu/+source/rkward>
  - Debian:
    <http://bugs.debian.org/cgi-bin/pkgreport.cgi?package=rkward>
  - Fedora:
    <https://bugzilla.redhat.com/buglist.cgi?component=rkward&product=Fedora>

### Friendly mailing list archive

  - <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/index.html>
    is much easier to use than the "official" list archive (for
    rkward-devel, only, at this time).
