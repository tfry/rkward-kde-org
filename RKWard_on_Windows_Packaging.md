---
layout: page
---

# Packaging RKWard for Windows

## Out of date warning

The information on this page has not been fully tested for KF5, yet, but
should mostly work. It's targeted at developers or power users willing
to solve problems themselves, not to end users\!

RKWard for Windows is currently distributed in two different variants:
as a single installer, and as an installation bundle including R and
KDE. This page details the steps needed to create these two packages.

## Creating the RKWard for Windows installer

### Requirements

  - A source-based installation of KDE, using craft. In general you
    should *not* use the latest and greatest KDE, here, but the latest
    version which already has an official KDE on Windows release to it.

### Procedure

1.  Set up your KDE environment in your craft install.
2.  Check out a clean copy of the RKWard source from git. When creating
    the installer package, you will probably want to check out a release
    branch, rather than trunk. Thus, typically you will use something
    like:
      -

            :git clone git://anongit.kde.org/rkward.git
            :git checkout releases/0.7.0

        It does not really matter, where you put the sources. It may be
        a good idea to do so outside the craft tree, though.
3.  CD to the "windows_nsis"-subdirectory of the sources.
4.  There is a batch file "make_release.bat", here. Edit this (e.g.
    using kwrite). Adjust the top few lines to reflect the details of
    you installations. Follow these guidelines:
      - If `K:\K\bin\kwrite.exe` exists, set
    <!-- end list -->
      -

          -
            `KDEPREFIXDRIVE=K:`
            `KDEPREFIX=K`
            i.e. point this at the root of your emerge tree.
    <!-- end list -->
      - If `C:\R\R-x.y.z\bin\R.exe` exists (and you want to compile
        against that version), set
    <!-- end list -->
      -

          -
            `RHOMEDRIVE=C:`
            `RHOME=R/R-x.y.z`
    <!-- end list -->
      - If `C:\NSIS\makensis.exe` exists, set
    <!-- end list -->
      -

          -
            `MAKENSIS="C:/NSIS/makensis.exe"`
    <!-- end list -->
      - You'll probably take MinGW from your emerge tree, i.e.
    <!-- end list -->
      -

          -
            `MINGW_PATH="K:\K\mingw\bin"`
5.  Run
      -

            make_release.bat
6.  If all goes well, an .exe-file (eg: install_rkward_0.7.0.exe) of
    roughly 1.4MB will be created. Test this installer, by simply
    running it.

This should be it.

## Creating the installation bundle

1.  You will probably want to edit the rkward blueprint to refer to the
    release branch:

kwrite
CRAFT\\ROOT\\etc\\blueprints\\locations\\craft-blueprints-kde\\extragear\\rkward\\rkward\\rkward.py

1.  First compile

craft -i rkward

1.  Packaging is easy:

craft --package rkward
