---
layout: page
---

# RKWard development source repository / development snapshots

Instructions to track the most recent development version in our source
repository (git). This is of interest both to people wanting to be on
the bleeding edge of RKWard development, and to people wanting to
contribute patches.

## Alternatives: Daily builds

Sometimes all you want is to test a relatively recent development
snapshot, easily.

\- For Ubuntu users, a variety of PPAs is available at
[1](https://launchpad.net/~rkward-devel), including the following daily
build archives:

` - `[`KF5``   ``version``   ``of``
 ``RKWard`](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily)
` - `[`KF5``   ``version``   ``of``   ``RKWard``   ``compiled``
 ``for``   ``CRAN``
 ``R`](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily-cran)
` - `[`KF5``   ``version``   ``of``   ``RKWard``   ``compiled``
 ``for``   ``CRAN``   ``R``   ``and``   ``KF5``
 ``backports`](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-backports-daily-cran)

\- For Windows users, daily builds can be found at
<https://binary-factory.kde.org/job/RKWard_Nightly_mingw64/>

## Pre-requisites

### Generic requirements

You need a git client; on Debian based systems:

`$ apt-get install git`

Further, you need the KDE/Qt libraries and headers and R. See [the
Requirements](Building_RKWard_From_Source#Requirements.html)
section on the
[Building_RKWard_From_Source](Building_RKWard_From_Source.html)
page.

## Checking out the current Git sources

For anoynmous (read-only) access, use

`$ git clone `<git://anongit.kde.org/rkward.git>` rkward_devel`

This will download the development sources to a directory called
**rkward_devel**.

## Compiling

Proceed with compilation / installation as usual (see [Building RKWard
From Source](Building_RKWard_From_Source.html)). Make sure the
path provided to cmake is correct (for example, depending on where you
create the build directory, **'../rkward**' may be appropriate instead
of **'..**').

## Staying up to date

To update your working copy to the most recent changes, go to that
directory, and run

`$ git pull --rebase`

After this, generally only the

`$ make`

and

`$ sudo make install`

steps are needed (less, if you use some advanced tricks).

## Producing patches

Our preferred mechanism of accepting patches is via "phabricator"
([instructions](https://community.kde.org/Infrastructure/Phabricator)).

However, especially for simple patches, you can also submit them to the
[mailing list](Contact.html). In general, follow this procedure:

1.  Follow the instructions given above
2.  run git pull --rebase
3.  Make your changes directly in your clone of the repository
4.  Maybe run *git pull --rebase* again, to make sure changes other
    people made don't conflict with your changes
5.  run git diff \> patch.diff
6.  submit the diff on <http://phabricator.kde.org>, or send it to the
    mailing list with a short message, on what you did, and why.

## Source browsing

You can also browse the Git repository online:
<https://cgit.kde.org/rkward.git>

