---
layout: page
---

# Getting started translating RKWard

## General info

The primary ressource for RKWard translations is <http://l10n.kde.org/>.
A lot of documenation, and contact info for your language team is
available, there.

Note that the translation files are divided into the strings for the (core) application, and
several separate catalogs for plugins and help pages. See below for some hints.

## Translating RKWard the (core) application

### Testing your translation

To test your translation, copy the .po file you created/modified to the
"i18n"-subdirectory of your rkward sources. Then do cmake, make, make
install as detailed in the INSTALL instructions.

To switch between languages without changing the language for your
entire desktop (or to use a language other than your desktop default),
invoke rkward with

```LANG=xx rkward```

(where xx would be your language code, e.g. fr).

On some systems it appears to be necessary to specify *both* KDE_LANG
and LC_ALL. E.g.

```LC_ALL=de_DE.utf-8 KDE_LANG=de rkward```

## Translating RKWard plugins and help pages

Plugins and Help Pages are translatable since RKWard version 0.6.3. See
<http://api.kde.org/doc/rkwardplugins/i18n.html> for details.

## Developers: Maintainance of translations

Synchronize translations from KDE l10n to git like this:

1.  Clone (or update) the existing translation export repo:
  ```git clone git://anongit.kde.org/scratch/tfry/rkward-po-export i18n/po```
2.  Run
  ```scripts/import_translations.py```
(without language arg) to fetch updates
3.  git commit & push in the export repo.
Don't forget to announce upcoming releases, early, on
kde-i18n-doc@kde.org .
