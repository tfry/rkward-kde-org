---
layout: page
---

# Installing RKWard on Linux/Unix: Binaries and build instructions

This page provides information on the various distributions that have already
included RKWard or for which simple build instructions / scripts exist.
Obviously, packages do not exist for all common distributions, yet, and
some packages are outdated. Instructions on building from source are
available: [Building RKWard From Source](Building_RKWard_From_Source.html)

* 
{:toc}


## Debian & derivatives

RKWard is now included in the official debian distribution. Due to the
nature of the debian release cycle, inclusion in the testing and stable
archives may take a lot of time, and even debian unstable may not
include new releases, immediately.

More up to date packages are available. The following apt-lines (add to
/etc/apt/sources.list) may be of interest to you:

### deb-src for all systems

`# the latest source packages for all debian distributions`
`deb-src `<https://files.kde.org/rkward/debian>` ./`

With the above line in /etc/apt/sources.list , the following series of
commands should be enough to build and install your own binary .deb.
*This should work on all .deb-based systems, including Ubuntu* (if it
does not, [let us know](Contact.html)):

`# update package cache`
`sudo apt-get update`
`# fetch build dependencies`
`sudo apt-get build-dep rkward`
`# fetch and build sources`
`apt-get source -b rkward`

`# finally, install the resulting package`
`# its name should look something like "rkward_[VERSION]_[ARCH].deb", e.g.:`
`sudo dpkg -i rkward_0.6.5-1_amd64.deb`

### Official repositories

Using the official repositories on Debian based systems (such as
Ubuntu), RKWard can be easily installed by

`$ apt-get install rkward`

Currently available versions on the official repositories:
[Debian](http://packages.debian.org/search?keywords=rkward&searchon=names&exact=1&suite=all&section=all)
and
[Ubuntu](http://packages.ubuntu.com/search?keywords=rkward&searchon=names&exact=1&suite=all&section=all)

### Inofficial Ubuntu repositories

RKWard is available for Ubuntu in a variety of versions from the
[Launchpad project page](https://launchpad.net/rkward):

  - If you are using the version R shipped with Ubuntu by default:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-dailys>
  - If you are using the CRAN version of R:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable-cran>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel-cran>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-dailys-cran>

## Gentoo

This appears to be the central place for information about the rkward
package in Gentoo:
<http://packages.gentoo.org/package/sci-mathematics/rkward>

Note that the most recent release of RKWard is often masked testing on
x86 and amd64 in the official portage tree. To install, add the keyword
to your package.keywords and pull it in using emerge:

`# echo "sci-mathematics/rkward" >> /etc/portage/package.keywords`
`# emerge -av rkward`

In most cases you are done at this point and ready to use RKWard. As an
alternative you can also build RKWard from SVN, but be warned that not
every snapshot is guaranteed to build. To build from the latest SVN you
can put this ebuild into your local portage overlay (e.g.
/usr/local/portage/sci-mathematics/rkward-9999.ebuild):

    # Copyright 1999-2009 Gentoo Foundation
    # Distributed under the terms of the GNU General Public License v2
    # $Header: $


    EAPI="2"

    inherit kde4-base

    DESCRIPTION="An IDE/GUI for the R-project"
    HOMEPAGE="https://rkward.kde.org/"
    ESVN_REPO_URI="git://anongit.kde.org/rekonq/rkward"
    GIT_ECLASS="git"

    LICENSE="GPL-2"
    SLOT="0"
    KEYWORDS="~amd64 ~x86"
    IUSE="debug"

    DEPEND=">=dev-lang/R-2.7.0
            dev-lang/php[cli]"
    RDEPEND="${DEPEND}
            !<=sci-mathematics/rkward-0.5.0b"

    src_install() {
            kde4-base_src_install
            # avoid file collisions
            rm -f "${D}"/usr/$(get_libdir)/R/library/R.css
            rm -f "${D}"/usr/share/apps/katepart/syntax/r.xml
    }

Then just use

`# emerge -av rkward`

to check out git and built RKWard from the latest sources.

## FreeBSD

  - Information on the FreeBSD port of RKWard is here:
    <http://www.freshports.org/math/rkward-kde4/>

The FreeBSD port is typically updated very quickly for new releases of
RKWard, and hence almost always up to date.

Core instructions provided:

  - To install the port:

`cd /usr/ports/math/rkward-kde4/ && make install clean`

  - To add the package:

`pkg_add -r rkward-kde4`

## SUSE / openSUSE

Starting with openSUSE 12.3, RKWard will be included in the official
distribution. To recommended way to get RKWard binaries for your
installation is using the package search:
<http://software.opensuse.org/search/find> .

The SUSE binaries are packaged by Detlef Steuer, and he has a lot more
useful links and information, at:
<http://fawn.hsu-hh.de/~steuer/?Dr._Detlef_Steuer:Projekte%2FProjects:Rkward_%28openSUSE%29>

*Sometimes* development snapshots get published at
<https://build.opensuse.org/project/show?project=home:tfry-suse:rkward-devel>
, but these are often *less* recent than the above binaries.

## Fedora

Generic installation:

`$ yum install rkward`

You can also find the build (of all versions of RKWard) at
[koji](http://koji.fedoraproject.org/koji/packageinfo?packageID=4881)
and other information regarding the package at
[pkgdb](https://admin.fedoraproject.org/pkgdb/packages/name/rkward).

## ArchLinux

A package is available here:
<https://aur.archlinux.org/packages.php?ID=4277>

## Slackware

Please use the R build script available at SlackBuilds.org in case your
installation of R doesn't include the R shared library, since it's
required by RKWard.

Slackware builds of RKWard seem not to be maintained, very actively, and
are often outdated. Search for "rkward" on <http://slackbuilds.org/> .
You may want to try compiling with the SlackBuild scripts from earlier
versions of RKWard, or earlier Slackware releases.

**13.1**: RKWard 0.5.3 slackbuild is located at
[SlackBuilds.org](http://slackbuilds.org/repository/13.1/academic/rkward/).

**13.0**: RKWard 0.5.3 slackbuild is located at
[SlackBuilds.org](http://slackbuilds.org/repository/13.0/academic/rkward/).

## Pardus

A package is available here:
<http://svn.pardus.org.tr/pardus/2011/devel/science/mathematics/rkward/>
. This is typically udpated very quickly after new releases.

## Mandriva

The Mandriva packaging often lags a bit behind. We recommend searching
rpmfind for the most recent available version:
<http://www.rpmfind.net/linux/rpm2html/search.php?query=rkward&submit=Search+>...\&system=Mandriva\&arch=

Somebody with knowledge on Mandriva, please correct / add information,
on where binary packages can be found. And esp. whether there is a
dynamic link always pointing to the most recent files.

## Windows

See [RKWard on Windows](RKWard_on_Windows.html) for details.

## Mac OS

See [RKWard on Mac](RKWard_on_Mac.html) for details.
