---
layout: page
---

# Screenshots

On this page:
* Table of contents
{:toc}

## Main Window

Startup dialog

![StartupSelection.png](/assets/img/StartupSelection.png)

The main application window with an introductory help page. At the left and there are several buttons to expand / collapse tool windows.

![MainWindow.png](/assets/img/MainWindow.png)

The application window with the Workspace Browser, Data Editor, File Browser, and RConsole tabs expanded and attached to the main window. The placement can be re-arranged, and each individual window can also be detached as a separate top level window (Windows > Detach).

![RKWardApplicationCombined.png](/assets/img/RKWardApplicationCombined.png)

The application window with tabs detached from the main window. They can be attached back to the main window: Window > Attach to main window.

![RKWardApplicationDetached.png](/assets/img/RKWardApplicationDetached.png)

## Browsing / Editing Data

The integrated editor allows to edit data.frames. Invalid cells are marked up. The workspace browser on the left allows you to see the objects in your workspace at a glance.

![EditorDataFrame.png](/assets/img/EditorDataFrame.png)

Other types of objects are not yet editable, but basic information on almost all data objects is available from a simple context menu.

![ViewObject.png](/assets/img/ViewObject.png)

## Editing Scripts

RKWard offers comfortable editing of R scripts using a built in editor. This is based on the powerful kate editor and offers - among others - syntax highlighting, object name completion, function argument hinting, and the ability to define and run various sections of code with a keyboard shortcut.

![ScriptEditor1.png](/assets/img/ScriptEditor1.png)

Example of object name completion

![ScriptEditor2.png](/assets/img/ScriptEditor2.png)

## The R Console

The R Console window offers direct interaction with the R interpreter, much like a regular R Console. Here, the console window has been detached from the main application window. All tool and document windows can be separated from and reattached to the main window.

![RConsole.png](/assets/img/RConsole.png)

## Graphics Windows

RKWard adds some helpful menu options to R's graph windows. This plot is one of those produced by "demo (graphics)" in R.

![ScreenDevice.png](/assets/img/ScreenDevice.png)

RKWard provides and easy way to create new devices (Device > Duplicate) and switch back and forth between them (Device > Make active).

![MultiDevices.png](/assets/img/MultiDevices.png)

A unified plugin for exporting the graphics devices to multiple formats.

![GraphicsExport1.png](/assets/img/GraphicsExport1.png)

Further output specific options.

![GraphicsExport2.png](/assets/img/GraphicsExport2.png)

## Configuring and Loading Packages

Many, many add-on packages can be downloaded for R. RKWard offers a graphical interface to browse, install, and update such packages.

![ConfigurePackages.png](/assets/img/ConfigurePackages.png)

This dialog allows to easily load/unload multiple packages simultaneously.

![LoadPackages.png](/assets/img/LoadPackages.png)

## Example Dialogs: T-Test

This is an example of a more complex plugin. This provides a visualization of the Central Limit Theorem for the Beta distribution. The Preview graph window allows you to see the effect of your changes directly.

![ExamplePluginStep1.png](/assets/img/ExamplePluginStep1.png)

Internally, the statistical plugins simply generate R code according to your settings. You can view the generated code as you make changes, and copy-and-paste it to your own scripts.

![ExamplePluginStep2.png](/assets/img/ExamplePluginStep2.png)

## Example Dialogs: Beta CLT

This is an example of a more complex plugin. This provides a visualization of the Central Limit Theorem for the Beta distribution. The Preview graph window allows you to see the effect of your changes directly.

![BetaCLT.png](/assets/img/BetaCLT.png)

## Third party extensions

RKWard can be extended by third party plugins. The following is a screenshot from the comprehensive and highly recommendable [rk.Teaching-package](http://aprendeconalf.es/rkteaching/).

![rkTeaching_Boxplot.png](/assets/img/rkTeaching_Boxplot.png)


## Output Window

Most plugins send their output to the Output Window, and you can also add text and graphics to the output window from custom R scripts. We are aware, this window needs a lot of improvements, but it already allows you to create a simply documentation of your anaylses, easily.
The Run again-link allows you to quickly invoke the plugin again, with the same settings. This allows you to redo an analysis with new data or slightly changed options in an easy and straightforward way.

![OutputWindow.png](/assets/img/OutputWindow.png)

Graphics devices can be copied to the Output (Device > Copy to output) from where it can be easily dragged into an office application, such as, Openoffice.org Writer. This way exporting a R graphics and importing it into the Writer is handled on the fly!

![GraphicsOutputWriter.png](/assets/img/GraphicsOutputWriter.png)


## Settings Window

![ConfigureRKWard1.png](/assets/img/ConfigureRKWard1.png)

## Menubar

File Menu

![Menubar01_File.png](/assets/img/Menubar01_File.png)

Edit Menu

![Menubar02_Edit.png](/assets/img/Menubar02_Edit.png)

Workspace Menu

![Menubar03_Workspace.png](/assets/img/Menubar03_Workspace.png)

Run Menu

![Menubar04_Run.png](/assets/img/Menubar04_Run.png)

Anaylsis Menu

![Menubar05_Analysis.png](/assets/img/Menubar05_Analysis.png)

Plots Menu

![Menubar06_Plots.png](/assets/img/Menubar06_Plots.png)

Distributions Menu

![Menubar07_Distributions.png](/assets/img/Menubar07_Distributions.png)

Windows Menu

![Menubar08_Windows.png](/assets/img/Menubar08_Windows.png)

## Graphics history actions

RKWard keeps a history of previous plots, allowing you to go back, easily.

![GraphicsHistory_svn_01.png](/assets/img/GraphicsHistory_svn_01.png)
