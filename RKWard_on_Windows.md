---
layout: page
---

# Download and install RKWard on Windows

On this page:
* Table of contents
{:toc}

## Installing the official release

Since version 0.7.0 installation of RKWard on windows should be
straight-forward: Simply download and run the [latest installer](https://download.kde.org/stable/rkward/0.7.0/win32/rkward-0.7.0b-R-3.4.4-KF5-5.43.exe).
The installer includes RKWard 0.7.0b, R 3.4.4, and all required KF5
frameworks (version 5.43).

For installing older, KDE4-based versions of RKWard, refer to the
instructions an an [earlier version of this page](https://web.archive.org/web/20160314173239/https://rkward.kde.org/RKWard_on_Windows).

## Installing development snapshots

Development snapshot are available from
<https://binary-factory.kde.org/job/RKWard_Nightly_mingw64/> . Note that
these are built, automatically, and provided without any human testing.
Back up your data, before using these.

## Updating R inside the RKWard installation

To update R, use the R installer.

  - Often, new releases of R will work with old versions of RKWard,
    especially, for patch releases (those differing only in the third
    section of the version number), generally there should not be any
    problems. However, it is always a good idea to use the latest
    version of RKWard with the latest version of R. If in any doubt,
    back up your installation, first.
  - You will need to install to
    `[Your\RKWard\Installation\]KDE\libs\R` .
  - Remember to update your R packages after updating R.

## Known issues and work-arounds

(Please use the [bug trackers](Bugs.html) to report issues.
This section is only meant to document some deficiencies, which are
encountered relatively frequently.)

  - Some versions of the Symantec **virus scanner** are known to report
    false positives for "knotify4.exe" and "keditfiletype.exe", based on
    heuristic detection. If in any doubt, please contact your virus
    scanner's vendor.

If you run into issues not listed above, or you can provide further
insight on the above issues, please do not hesitage to
[contact](Contact.html) us\!

## Compiling RKWard from source on Windows

1.  Set up "craft" as detailed, here:
    <https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows>
    .
2.  run

`craft -i rkward`

This should be all you need to fetch rkward and all dependencies
(including R). Note that this may take several hours to complete.

#### Notes

  - Currently this has only been tested with the MinGW(4) compiler. You
    are encouraged to try compiling using MSVC, but quite likely you
    will run into some problems. Report those on the [mailing
    list](Contact.html), and we will take care of them.

<!-- end list -->

  - At times, compilation using craft will fail. If the build fails on
    the R or RKWard packages themselves, let us know on the [mailing
    list](Contact.html). If the build fails at an earlier stage
    (qt / kdelibs / kdebase), you may want to ask for help on
    \[kde-windows@kde.org the KDE windows mailing list\].

<!-- end list -->

  - To customize your installation of R or RKWard, edit / refer to the
    following python files
      - *CRAFT\\ROOT*\\etc\\blueprints\\locations\\craft-blueprints-kde\\extragear\\rkward\\rkward\\rkward.py
      - *CRAFT\\ROOT*\\etc\\blueprints\\locations\\craft-blueprints-kde\\binary\\_win\\r-base\\r-base.py

### Custom compilation / Packaging (**not** recommended for most users)

For more information, refer to the [developers' notes on packaging RKWard for Windows](RKWard_on_Windows_Packaging.html).

### Debugging / further info

See
<http://techbase.kde.org/Development/Tutorials/Debugging/Debugging_on_MS_Windows>
in the KDE wiki.
