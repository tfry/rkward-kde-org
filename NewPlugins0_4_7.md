---
layout: page
---

# Plugins introdoced or reworked in RKWard 0.4.7

### Plots

  - Pareto Chart
      - Status: Complete
      - Tested by (when): tfry / 2007-03-16
      - Responsible: nono_231

<!-- end list -->

  - Stem and leaf plot
      - Status: Complete
      - Responsible: sjar
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Histogram
      - Status: Complete
      - Responsible: tfry
      - Tested by (when): tfry / 2007-03-16
      - Notes: Modified using histogram_options.

<!-- end list -->

  - ECDF plot
      - Status: Complete
      - Responsible: tfry
      - Tested by (when): tfry / 2007-03-18
      - Note: Modified to use plot_stepfun_options.

<!-- end list -->

  - Generic plot
      - Status: Complete
      - Tested by (when): tfry / 2007-04-09
      - Responsible: pk

#### Embedded

  - Step function plot options (plot_stepfun_options)
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25
      - Note: This plugin is for embedding.

<!-- end list -->

  - Histogram options (histogram_options)
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-16
      - Note: This plugin is for embedding.

### Distributions

#### CLT plots

  - Binomial CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Geometric CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Hypergeometric CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Negativ binomial CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Poisson CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Wilcoxon CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Beta CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Chi-squared CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Exponential CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - F CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Gamma CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Logistic CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Log normal CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Normal CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - t CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Uniform CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

<!-- end list -->

  - Weibull CLT plot
      - Status: Complete
      - Responsible: pk
      - Tested by (when): tfry / 2007-03-25

### Workspace

  - Set working directory
      - Status: Complete
      - Tested by (when): tfry / 2007-03-16
      - Responsible: pk

### X11

  - Export X11 Device
      - Status: Complete
      - Tested by (when): tfry / 2007-03-16
      - Responsible: pk
      - Notes: Has replaced the old Export X11 Device plugin

<!-- end list -->

  - Add grid (X11 \> Edit \> Draw Grid)
      - Status: Complete
      - Tested by (when): tfry / 2007-03-16
      - Responsible: pk

### Analysis

#### Variances

  - Bartlett test
      - Status: complete
      - Tested by (when): tfry / 2007-03-27
      - Responsible: sjar

<!-- end list -->

  - F test
      - Status: complete
      - Tested by (when): tfry / 2007-03-27
      - Responsible: sjar

<!-- end list -->

  - Kligner-Killeen test
      - Status: complete
      - Tested by (when): tfry / 2007-03-27
      - Responsible: sjar

#### Moments

  - Skewness and Kurtosis
      - Status: complete
      - Tested by (when): tfry / 2007-03-25
      - Responsible: sjar

<!-- end list -->

  - Statistical Moment
      - Status: complete
      - Tested by (when): tfry / 2007-03-25
      - Responsible: sjar

<!-- end list -->

  - D'Agostino test of skewness
      - Status: complete
      - Tested by (when): tfry / 2007-03-21
      - Responsible: sjar

<!-- end list -->

  - Anscombe-Glynn test of kurtosis
      - Status: complete
      - Tested by (when): tfry / 2007-03-21
      - Responsible: sjar

<!-- end list -->

  - Bonett-Seier test of Geary's kurtosis
      - Status: complete
      - Tested by (when): tfry / 2007-03-21
      - Responsible: sjar

#### Outliers

  - Dixon test
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar

<!-- end list -->

  - Grubbs test
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar

<!-- end list -->

  - Outlier
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar

<!-- end list -->

  - Chi-squared test for outlier
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar

#### Misc. tests

  - Phillips-Perron Test for Unit Roots
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar

<!-- end list -->

  - Mood Two-Sample Test of Scale
      - Status: complete
      - Tested by (when): tfry / 2007-04-01
      - Responsible: sjar
