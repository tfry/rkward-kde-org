---
layout: page
---

# Release Schedule

**No release is currently scheduled. This page shows the schedule for RKWard 0.7.0** For up-to-date development snapshots, refer to [this page](/RKWard_Source_Repository.html).

## Note

All dates on this page are subject to change with or without prior
notice. We put up this schedule to coordinate release efforts, but not
to enslave ourselves to a plan.

## RKWard 0.7.0 (the first version to be based on version 5 of the KDE libraries)

### Feature Plan

[Feature Plan](Feature_Plan.html)

### Development snapshots

Several options exist for following the [development version of
RKWard](RKWard_SVN.html).

### Tentative timeline

#### Preview release(s)

A preview release (0.7.0-pre1) will become available roughly April 6,
2018. More preview releases may be created during the testing phase on
an as needed basis.

  - Call for testing:
    [1](https://mail.kde.org/pipermail/rkward-devel/2018-April/004916.html).
  - [Source
    tar.gz](https://files.kde.org/rkward/testing/rkward-0.6.9z+0.7.0+pre1.tar.gz)
    (see [Building RKWard From
    Source](Building_RKWard_From_Source.html) for instructions).
  - Links to binary packages (not all may be available, immediately):
      - [2](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily)
        or [Ubuntu with CRAN version of
        R](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily-cran)
      - [Windows binary
        installer](https://files.kde.org/rkward/testing/bundles/Windows/rkward-0.7.0-pre1-mingw_64-gcc.exe)
        (see [RKWard on Windows](RKWard_on_Windows.html) for
        instructions).
      - [MacOSX binary installation
        bundle](https://files.kde.org/rkward/testing/bundles/MacOSX/RKWard-binary-0.7.0pre1_OSX10.11_KF5-5.42.0_needs_CRAN_R-3.4.4.pkg)
        (see [RKWard on Mac](RKWard_on_Mac.html) further
        information - you need to install R 3.4.4, separately).

#### Testing phase

The testing phase will be open until April 13, 2016. The phase may be
extended, if needed, but please try to provide all substantial testing
feedback by this date.

#### Deadline for translations

The deadline for translations ends with the testing phase, on April 13,
2018. Please supply your translations before this date. See
<http://l10n.kde.org> for information on how to get involved with
translations. Contact us on the mailing list, if you need assistance.

#### Release candidate / packaging

Subject to the outcome of the testing phase, a 0.7.0-rc1-release will be
prepared on or around April 14, 2018, and will appear in
<http://files.kde.org/rkward/testing/for_packaging/>. Packagers are
encouraged to start producing packages based on this release (with
details to be announced on the mailing list).

The intent is to release the final version with no change with respect
to the release candidate (except for the version number). Only grave
bugs will prompt another rc-release. Minor bugs reported after the
rc1-release will have to wait for the next release cycle.

**Packagers**: If you intend to publish packages before the official
release, please make sure that the version number contains some
indication that this is not the "official" version, yet (e.g. a "-rc1"
suffix). Otherwise, if we do need to make a last-minute change to fix an
important problem, this may result in different versions floating with
the same version number, and serious confusion.

#### Official release

The targeted official release date for RKWard 0.7.0 is April 16, 2018.

## Development snapshots

  - Daily builds for Ubuntu:
    <https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily>
  - Windows installer (including R and KDE; \~140 MB):
    <https://files.kde.org/rkward/testing/bundles/Windows/rkward-0.7.0-pre1-mingw_64-gcc.exe>

[Category:Developer
Information](Category:Developer_Information.html)
