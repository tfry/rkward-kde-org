---
layout: page
---

# Building RKWard from Source

The purpose of this page is to give you step-by-step instructions to
compile and install RKWard from sources.

**Note**: These are generic instructions, which should work on most
systems. However, for many systems, more detailed instructions, or even
ready-made binary packages are available. See
[Binaries_and_Build_Scripts](Binaries_and_Build_Scripts.html). For building on Windows, see [RKWard_on_Windows/Packaging](RKWard_on_Windows/Packaging.html). 

**Note**: For instructions on compiling the KDE 4 versions of RKWard
(before RKWard 0.7.0), please refer to [an older version of this page](https://web.archive.org/web/20170324162924/https://rkward.kde.org/Building_RKWard_From_Source).

## Contents of this page:

* Table of contents
{:toc}

## Getting the source code

  - [Available source packages](Download_RKWard.html)
  - [Getting the source from git](RKWard_Source_Repository.html)

## Building RKWard

### Requirements

RKWard requires:

  - KF5-libraries and headers (\>= 5.2) (http://www.kde.org)
      - The names of the required packages differ from distribution to
        distribution. On Debian and Ubuntu the name is "libkf5**XYZ**".
        Installing "libkf5webkit-dev" and "libkf5texteditor-dev" should
        automatically install the required dependencies.
  - Qt-libraries and headers (\>= 5.2) (http://www.qt.io)
      - On most systems these should be installed automatically, when
        you install the KF5-libraries, above.
  - R and headers (http://www.r-project.org)
      - Common package names are "r", "r-base", etc. Note that some
        distributions package the HTML-documentation in a separate
        package, so look out for "r-base-html" or similar package names.
      - If you compile R from source, make sure to use the
        "--enable-R-shlib" option to configure.
  - CMake (http://cmake.org)
      - Generally this package is called "cmake". You will also need
        "extra-cmake-modules (ECM)".

If you can't find the required packages for your distribution, take a
look at
[Binaries_and_Build_Scripts](Binaries_and_Build_Scripts.html)
for distribution specific information, or [contact us](Contact.html) for help.

Debian derivatives:

  - On Debian based systems (such as Ubuntu) the following will get all
    the dependencies installed (and hopefully the package names will be
    similar on other distributions):

$ apt-get install libkf5webkit-dev libkf5texteditor-dev kdoctools-dev
r-base-dev cmake extra-cmake-modules

### Compilation

IMPORTANT: You will need to have the R shared library installed. This is
typically located in a place like /usr/lib/R/lib/libR.so. Unfortunately,
some distributions do not yet ship their R binary with the shared
library. In those (rare) cases, you'll have to compile and install R on
your own with the option --enable-R-shlib.

Suppose the source is obtained as (here **rkward-0.7.0.tar.gz** is used
as an example, change it accordingly): $ tar xzf rkward-0.7.0.tar.gz \#
extracts to **rkward-0.7.0** directory $ cd rkward-0.7.0

Then, the recommended way to build from source is: $ mkdir build; cd
build $ cmake .. \[options\] \# see *Path to sources* and *CMake
options* below $ make $ sudo make install

#### Path to sources

The cmake command needs to know the path to rkward sources. Typically
this where the CMakeLists.txt file exists. Since, in the example above,
the build directory was created inside the extracted source directory,
CMakeLists.txt exists in the directory immediately above; hence **'..**'
is the appropriate path.

#### CMake options (configuring)

The cmake command offers a number of parameters to control the build
process. Some commonly needed are:

  - \-DCMAKE_INSTALL_PREFIX
      - set the installation base directory. Generally
        -DCMAKE_INSTALL_PREFIX=\`kde5-config --prefix\` should be
        correct.

<!-- end list -->

  - \-DCMAKE_BUILD_TYPE
      - type of build. Useful settings include
        -DCMAKE_BUILD_TYPE=Release and -DCMAKE_BUILD_TYPE=debugfull

In some cases you may want to set the following options:

  - \-DR_EXECUTABLE
      - path to the R executable. You may want to set this, if you have
        multiple versions of R installed, or R is not in your path.
        Example: -DR_EXECUTABLE=/usr/bin/R
  - \-DR_LIBDIR
      - directory where R packages should be installed. This option is
        mainly of interest to packagers.
      - When installing as a normal user (i.e., w/o root access), this
        needs to be explicitely specified. Generally this is the
        R_LIBS_USER variable from the Renviron file which can be
        identified from withing R using:

$ R --silent \> Sys.getenv ("R_LIBS_USER").

`                    R_LIBS_USER`

"\~/R/i486-pc-linux-gnu-library/2.9"

  -

      -
        For example, on a i386 Debian with R 2.9,
        \~/R/i486-pc-linux-gnu-library/2.9 is the default. Create the
        directory manually, if it doesn't exist: **mkdir -p
        \~/R/i486-pc-linux-gnu-library/2.9** . Then, using
        -DR_LIBDIR=$HOME/R/i486-pc-linux-gnu-library/2.9 should work.

<!-- end list -->

  - \-DR_HOME
      - R home directory. Almost certainly this option is not needed.
  - \-DR_INCLUDEDIR
      - path to R include files. Almost certainly this option is not
        needed.

Further generic options are listed on
<http://www.cmake.org/Wiki/CMake_Useful_Variables> .

### Installation

As a last step, you _have to_ run 'make install'. Otherwise RKWard
will be missing important menu-options, may behave strangely, or may not
run at all. As root run: $ make install

Make sure you have set the correct path in cmake (the
-DCMAKE_INSTALL_PREFIX option).

### Running

For now it's recommended to run RKWard from the command-line for
debug-output. For the few available command-line options see

$ rkward --help

## Troubleshooting

The most common source of problems are

1.  Missing some of the build requirements
2.  Wrong / missing parameters to cmake

So please re-read the corresponding sections, first. If you're still in
trouble, [let us know](Contact.html), we'll be happy to help you.

For some systems,
[Binaries_and_Build_Scripts](Binaries_and_Build_Scripts.html)
has more information.
